from datetime import datetime
from sqlalchemy import insert, select
from httpx import AsyncClient
from cft_test.auth.models import tokens, users
from conftest import async_session_maker


async def test_add_user():
    """Test adding a user to the database."""
    date = datetime.strptime(str(datetime.now()), '%Y-%m-%d %H:%M:%S.%f')

    async with async_session_maker() as session:
        stmt = insert(users).values(
            id=1,
            email="random_email@yahoo.com",
            hashed_password="dvwdkjHE1YV1Vvuiv444ry4r$24y@Y$V@$K@HB$@$VU@$B@H$V@$@V$VHB@J$",
            registered_at=date,
            is_active=True,
            is_superuser=False,
            is_verified=False,
            surname="malyanov",
            name="matvey")
        await session.execute(stmt)
        await session.commit()

        query = select(users)
        result = await session.execute(query)
        assert result.all() == [(
            1, "random_email@yahoo.com",
            "dvwdkjHE1YV1Vvuiv444ry4r$24y@Y$V@$K@HB$@$VU@$B@H$V@$@V$VHB@J$",
            date,
            True,
            False,
            False,
            "malyanov",
            "matvey")]


async def test_add_token():
    """Test adding a token to the database."""
    async with async_session_maker() as session:
        stmt = insert(tokens).values(
            id=1,
            access_token="dvwdkjHE1YV1Vvuiv444ry4r$24y@Y$V@$K@HB$@$VU@$B@H$V@$@V$VHB@J$",
            user_id=1)
        await session.execute(stmt)
        await session.commit()

        query = select(tokens)
        result = await session.execute(query)
        assert result.all() == [
            (1,
             "dvwdkjHE1YV1Vvuiv444ry4r$24y@Y$V@$K@HB$@$VU@$B@H$V@$@V$VHB@J$",
             1)]


async def test_api_login_success(ac: AsyncClient):
    """Test API login success."""
    response = await ac.post("/login", json={
        "email": "random_email@yahoo.com",
        "password": "dvwdkjHE1YV1Vvuiv444ry4r$24y@Y$V@$K@HB$@$VU@$B@H$V@$@V$VHB@J$",
    })

    assert response.status_code == 200


async def test_api_login_failed(ac: AsyncClient):
    """Test API login failure."""
    response = await ac.post("/login", json={
        "email": "broken_email@yahoo.com",
        "password": "dvwdkjHE1YV1Vvuiv444ry4r$24y@Y$V@$K@HB$@$VU@$B@H$V@$@V$VHB@J$",
    })

    assert response.status_code == 401


async def test_api_profile_success(ac: AsyncClient):
    """Test API profile access success."""
    response = await ac.get("/profile/1", headers={
        "Content-Type": 'application/json',
        "Authorization":
            "dvwdkjHE1YV1Vvuiv444ry4r$24y@Y$V@$K@HB$@$VU@$B@H$V@$@V$VHB@J$",
    })

    assert response.status_code == 200


async def test_api_profile_failed(ac: AsyncClient):
    """Test API profile access failure."""
    response = await ac.get("/profile/2", headers={
        "Content-Type": 'application/json',
        "Authorization":
            "dvwdkjHE1YV1Vvuiv444ry4r$24y@Y$V@$K@HB$@$VU@$B@H$V@$@V$VHB@J$",
    })

    assert response.status_code == 403
