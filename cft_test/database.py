from typing import AsyncGenerator

from sqlalchemy import MetaData
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from config import DATABASE_URL

# Создание базового класса для описания моделей
Base = declarative_base()

# Объект метаданных
metadata = MetaData()

# Создание асинхронного движка для работы с базой данных
engine = create_async_engine(DATABASE_URL)

# Создание фабрики сессий
async_session_maker = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    """
    Функция-генератор, возвращающая асинхронную сессию для работы с базой данных.

    Yields:
        AsyncSession: Асинхронная сессия SQLAlchemy.
    """
    async with async_session_maker() as session:
        yield session
