"""
    Настройка системы аутентификации и управления пользователями с использованием FastAPIUsers.

    Этот модуль настраивает систему аутентификации и управления пользователями в приложении FastAPI
    с использованием библиотеки fastapi-users.

"""

from fastapi_users import FastAPIUsers
from fastapi_users.authentication import CookieTransport, AuthenticationBackend
from fastapi_users.authentication import JWTStrategy

from stuff import get_user_admin
from models import Users
from cft_test.config import SECRET_AUTH


def get_jwt_strategy() -> JWTStrategy:
    return JWTStrategy(secret=SECRET_AUTH, lifetime_seconds=3600)


cookie_transport = CookieTransport(cookie_name="salary", cookie_max_age=3600)

auth_backend = AuthenticationBackend(
    name="jwt",
    transport=cookie_transport,
    get_strategy=get_jwt_strategy,
)

fastapi_users = FastAPIUsers[Users, int](
    get_user_admin,
    [auth_backend],
)
