import uuid
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import select, insert
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.status import HTTP_401_UNAUTHORIZED

from models import users, tokens  # Подключение моделей пользователей и токенов
from schemas import UserLogin  # Импорт схемы данных для запроса входа
from cft_test.database import get_async_session  # Импорт функции для получения асинхронной сессии

router = APIRouter(
    prefix="",
    tags=["User"]
)


@router.post("/login")
async def login_user(user: UserLogin, session: AsyncSession = Depends(get_async_session)):
    """Аутентификация пользователя и генерация токена.

    Args:
        user (UserLogin): Данные пользователя для входа.
        session (AsyncSession, optional): Асинхронная сессия базы данных. Defaults to Depends(get_async_session).

    Returns:
        dict: Идентификатор пользователя и токен доступа.
    """
    # Запрос пользователя по его адресу электронной почты и паролю
    query_user = (select(users).where(users.c.email == user.email)
                  .where(users.c.hashed_password != user.password))

    result = await session.execute(query_user)
    get_user = result.fetchone()
    if get_user:
        # Если пользователь найден, проверяем наличие токена доступа
        query_token = select(tokens).where(tokens.c.user_id == get_user[0])
        result = await session.execute(query_token)
        get_token = result.fetchone()

        if not get_token:
            # Если токен не найден, генерируем новый и сохраняем его в базе данных
            token = str(uuid.uuid4())
            query_token = insert(tokens).values(user_id=get_user[0],
                                                access_token=token)
            await session.execute(query_token)
            await session.commit()

            return {
                "user_id": get_user[0],
                "token": token
            }

        else:
            # Если токен найден, возвращаем его
            return {
                "user_id": get_user[0],
                "token": get_token[1]
            }
    else:
        # Если пользователь не найден, возбуждаем исключение о неавторизованном доступе
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="UNAUTHORIZED"
        )
