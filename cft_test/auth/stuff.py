from typing import Optional

from fastapi import Depends, Request
from fastapi_users import BaseUserManager, IntegerIDMixin

from models import Users
from utils import get_user_db

from cft_test.config import (SECRET_AUTH)


class UserAdmin(IntegerIDMixin, BaseUserManager[Users, int]):
    reset_password_token_secret = SECRET_AUTH
    verification_token_secret = SECRET_AUTH

    async def on_after_register(self, user: Users, request: Optional[Request] = None):
        print(f"User {user.id} is registered.")


async def get_user_admin(user_db=Depends(get_user_db)):
    yield UserAdmin(user_db)
