from typing import Annotated

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from fastapi.security import APIKeyHeader

from utils import get_user_by_token
from cft_test.database import get_async_session

apikey_scheme = APIKeyHeader(name="Authorization", auto_error=False)

router = APIRouter(
    prefix="",
    tags=["User"]
)


@router.get("/profile/{user_id}")
async def user_profile(user_id: int, access_token: Annotated[str, Depends(apikey_scheme)],
                       session: AsyncSession = Depends(get_async_session)):
    """
    Retrieve the profile of a user identified by user_id.

    Parameters:
    - user_id (int): The ID of the user whose profile is to be retrieved.
    - access_token (str, optional): The access token provided in the request header.
    - session (AsyncSession, optional): The asynchronous database session.

    Returns:
    dict: A dictionary containing the user's profile information.

    Raises:
    HTTPException: If the user profile cannot be found or if there is an authentication error.
    """
    return await get_user_by_token(user_id, access_token=access_token, session=session)
